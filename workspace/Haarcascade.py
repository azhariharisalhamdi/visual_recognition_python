import numpy as np
import cv2
import sys
import os

dataFace = cv2.CascadeClassifier('../data/haarcascades/haarcascade_frontalface_alt.xml')

image = temp_image = cv2.imread(sys.argv[1])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

faces = dataFace.detectMultiScale(gray, 1.1, 5)
for (x,y,w,h) in faces:
    cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
    temp_image = image
    crop= image[ y:h+y,x:w+x]

#cv2.imshow('detected image',temp_image)
#temp_file_name = os.path.splitext(sys.argv[1])[0]
file_name = os.path.basename(sys.argv[1])

cv2.imwrite('result_'+file_name, temp_image)
cv2.imwrite('cropped_'+file_name,crop)
cv2.waitKey(0)
cv2.destroyAllWindows()
