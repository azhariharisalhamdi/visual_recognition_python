import cv2
import sys
import os, errno

'''
	it will be generate croped face from ID in /repo/dataset/$username
'''

temp_video = cv2.VideoCapture(sys.argv[2])
file_name = os.path.basename(sys.argv[2])
username = sys.argv[1]

name = os.path.splitext(file_name)[0]
success,image = temp_video.read()
count = 0
success = True

#resultDir = r'../user/'+username+'/dataset' 
resultDir = r'../dataset/'+username 
try:
    os.makedirs(resultDir)
except OSError as e:
    if e.errno != errno.EEXIST:
        print ("file exist")
        raise
'''
if not os.path.exists(resultDir):
    os.makedirs(resultDir)
'''
#os.chdir('../user/'+username+'/dataset' )
os.chdir('../dataset/'+username )

while success:
  success,image = temp_video.read()
  if success==True :
  	print ("Generate a new frame: %d" % count)
  	cv2.imwrite(username+"_extract_frame_%d.jpg" % count, image)
  count += 1
