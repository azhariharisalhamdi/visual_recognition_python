import numpy as np
import cv2
import sys
import os, errno

'''
	it will be generate croped face from ID in /repo/dataset/$username
'''

dataFace = cv2.CascadeClassifier('../data/haarcascade_frontalface_alt.xml')
username = file_name = sys.argv[1]
image = temp_image = cv2.imread(sys.argv[2])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

#resultDir = r'../user/'+username+'/dataset' 
resultDir = r'../dataset/'+username 
try:
    os.makedirs(resultDir)
except OSError as e:
    if e.errno != errno.EEXIST:
        print ("file exist")
        raise
'''
if not os.path.exists(resultDir):
    os.makedirs(resultDir)
'''

faces = dataFace.detectMultiScale(gray, 1.1, 5)
for (x,y,w,h) in faces:
    cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
    temp_image = image
    crop= image[ y:h+y,x:w+x]

#cv2.imshow('detected image',temp_image)
#temp_file_name = os.path.splitext(sys.argv[2])[0]
#file_name = os.path.basename(sys.argv[2])

#os.chdir('../user/'+username+'/dataset' )
os.chdir('../dataset/'+username )

cv2.imwrite(username+'_face_ID_detection.jpg', temp_image)
cv2.imwrite(username+'_face_ID_result.jpg',crop)
